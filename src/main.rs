#![allow(unused_variables)]

pub mod omath;
mod model;
mod events;
mod test_basic;
pub mod v1;

fn main() {
  v1::test::main();
  // test_basic::test();
}
